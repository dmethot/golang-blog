---
title: "Golang"
date: 2020-06-02T20:16:26-04:00
draft: true
tags : [
    "go",
    "golang",
]
---

# Golang Hello World


In your favorite editor, write the follow snippet of code:
```go
package main

func main() {
    fmt.Println("Hello, World!")
}
```

You can then run via:
```shell
→ go run main.go
```
